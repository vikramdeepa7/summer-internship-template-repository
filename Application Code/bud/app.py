from flask import Flask,render_template,request
#import numpy as np
import sklearn.metrics as metrics
import numpy as np
from sklearn.neighbors import NearestNeighbors
from scipy.spatial.distance import cosine
from sklearn.metrics import pairwise_distances
import pickle

app = Flask(__name__)
model = pickle.load(open('model.pkl','rb'))

@app.route('/')
def index():
	return render_template('skillform.html')

@app.route('/predict', methods=['GET' , 'POST'])
def predict():
    if request.method =='POST':
        arr = (request.form.getlist('f'))
       
    final_features= [[len(arr),50]]
    prediction = model.predict(final_features)
    if prediction == 0:
    	str = "Here are the skills you can go through to make yourself sure about the project!! "
    else:
        str = "You are all set to work for the project!!"

    return render_template('result.html', prediction_text = str)

if __name__ == '__main__':
    app.run(debug=True)
