<?php 
  
  $errors = "";
  // connect to database
  $db = mysqli_connect("localhost", "root", "", "buddy");

  // insert a quote if submit button is clicked
  if (isset($_POST['submit'])) {

    if (empty($_POST['team']) || empty($_POST['work'])) 
    {
      $errors = "DON'T LEAVE IT BLANK!!!";
    }
    else
    {
      $team = $_POST['team'];
      $work = $_POST['work'];
      $query = "INSERT INTO project_1_work VALUES ('$team','$work')";
      mysqli_query($db, $query);
    }
  } 

  // select all tasks if page is visited or refreshed
  $project_1_work = mysqli_query($db, "SELECT * FROM project_1_work");

?>



<!DOCTYPE html>
<html>
<head>
  <title>Buddy</title>
  <link rel="stylesheet" type="text/css" href="proj1_work.css">
  <link rel="icon" href="logo.png.jpg">
</head>
<body>
  <h1>BUDDY</h1>
  <h2>Do Your Task Perfectly</h2> 
  <header>
        <nav >
          <ul >
            <li><a href="project1.php" class="home">TEAM DETAILS</a></li>
            <li><a href="proj1_details.php" class="home">PROJECT DETAILS</a></li>
            <li><a href="proj1_areas.php" class="home">FUNCTIONAL AREAS</a></li>
            <li><a href="proj1_work.php" class="home">WORK</a></li>
            <li><a href="proj1_chatroom.php" class="home">CHAT ROOM</a></li>
            <li><a href="proj1_settings.php" class="home">SETTINGS</a></li>
        </nav>
  </header>
  <div class="divider"></div>
  <div class="heading">
    <h4 class="pkpk">WORK</h4>
  </div>
  <form method="post" action="proj1_work.php" >
    <?php if (isset($errors)) { ?>
      <p><center><?php echo $errors; ?></center></p>
    <?php } ?>

    <div class="pro">
    <input type="text" name="team" class="task_input1" placeholder="Enter Your Team">
    <br/>
    <input type="text" name="work" class="task_input2" placeholder="Enter The Work Details">
    <br/>
    <button type="submit" name="submit" id="add_btn" class="add_btn">Add</button>
    </div>
  </form>
  

  <table>
    <thead>
      <tr class="title">
        <th class="s">Team Name</th>
        <th class="t">Work</th>
      </tr>
    </thead>

    <tbody>
      <?php $i = 1; while ($row = mysqli_fetch_array($project_1_work)) { ?>
        <tr class="attri">
          <td class="i"> <?php echo $row['team']; ?> </td>
          <td class="team"> <?php echo $row['work']; ?> </td>
        </tr>
      <?php $i++; } ?>  
    </tbody>
  </table>
  
</body>
</html>