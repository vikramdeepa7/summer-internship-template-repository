<?php 
  
  $errors = "";
  // connect to database
  $db = mysqli_connect("localhost", "root", "", "buddy");

  // insert a quote if submit button is clicked
  if (isset($_POST['features'])) 
  {

          if (empty($_POST['feature_'])) 
          {
            $errors = "DON'T LEAVE IT BLANK!!!";
          }
          else
          {
            $features = $_POST['feature_'];
            $sql="SELECT max(id) as id FROM features";
            $ival= mysqli_query($db, $sql);
            $irow = mysqli_fetch_array($ival);
            if(is_null($irow['id']))
              $i=1;
            else
              $i=$irow['id']+1;
            $query = "INSERT INTO features VALUES ('$i','$features')";
            mysqli_query($db, $query);
          }
  }
  else
  {
            if (isset($_POST['cons'])) 
            {

              if (empty($_POST['advantages_'])) 
              {
                $errors = "DON'T LEAVE IT BLANK!!!";
              }
              else
              {
                $cons = $_POST['advantages_'];
                $sql="SELECT max(id) as id FROM advantages";
                $ival= mysqli_query($db, $sql);
                $irow = mysqli_fetch_array($ival);
                if(is_null($irow['id']))
                  $i=1;
                else
                  $i=$irow['id']+1;
                $query = "INSERT INTO advantages VALUES ('$i','$cons')";
                mysqli_query($db, $query);
              }
            }
            else
            {
                if (isset($_POST['issues'])) 
              {

                if (empty($_POST['issues_'])) 
                {
                  $errors = "DON'T LEAVE IT BLANK!!!";
                }
                else
                {
                  $issues = $_POST['issues_'];
                  $sql="SELECT max(id) as id FROM issues";
                  $ival= mysqli_query($db, $sql);
                  $irow = mysqli_fetch_array($ival);
                  if(is_null($irow['id']))
                    $i=1;
                  else
                    $i=$irow['id']+1;
                  $query = "INSERT INTO issues VALUES ('$i','$issues')";
                  mysqli_query($db, $query);
                }
              }  
            
            }
  } 
  if (isset($_GET['del_task1'])) 
  {
      $id = $_GET['del_task1'];

      mysqli_query($db, "DELETE FROM features WHERE id=".$id);
      header('location: proj1_details.php');
  }
  else
  {
      if (isset($_GET['del_task2'])) 
      {
        $id = $_GET['del_task2'];

        mysqli_query($db, "DELETE FROM advantages WHERE id=".$id);
        header('location: proj1_details.php');
      }
      else
      {
          if (isset($_GET['del_task3'])) 
          {
            $id = $_GET['del_task3'];

            mysqli_query($db, "DELETE FROM issues WHERE id=".$id);
            header('location: proj1_details.php');
          }
      }

  }

  // select all tasks if page is visited or refreshed
  $projects = mysqli_query($db, "SELECT projectname FROM projects WHERE id = 1");
  $f_ = mysqli_query($db, "SELECT * FROM features");
  $ad_ = mysqli_query($db, "SELECT * FROM advantages");
  $iss_ = mysqli_query($db, "SELECT * FROM issues");

?>

<!DOCTYPE html>
<html>
<head>
	<title>Buddy</title>
	<link rel="stylesheet" type="text/css" href="proj1_details.css">
	<link rel="icon" href="logo.png.jpg">
</head>
<body>
  <h1>BUDDY</h1>
  <h2>Do Your Task Perfectly</h2> 
  <header>
        <nav >
          <ul >
          	<li><a href="project1.php" class="home">TEAM DETAILS</a></li>
          	<li><a href="proj1_details.php" class="home">PROJECT DETAILS</a></li>
          	<li><a href="proj1_areas.php" class="home">FUNCTIONAL AREAS</a></li>
            <li><a href="proj1_work.php" class="home">WORK</a></li>
          	<li><a href="proj1_chatroom.php" class="home">CHAT ROOM</a></li>
          	<li><a href="proj1_settings.php" class="home">SETTINGS</a></li>
        </nav>
	</header>
	<div class="divider"></div>
  <div class="heading">
    <h4 class="pkpk">
    <?php 
      $row = mysqli_fetch_array($projects);
      echo $row['projectname'];
    ?>
    </h4>
  </div>


  <form method="post" action="proj1_details.php" class="tud">
    <?php if (isset($_POST['features'])) 
          {
            if (isset($errors)) 
            { ?>
              <p><?php echo $errors; ?></p>
    <?php   }
          } ?>
    <p class="f">FEATURES</p>
    <input type="text" name="feature_" class="task_input1" placeholder="Enter Features">
    <button type="submit" name="features" id="add_btn1" class="add_btn1">Add</button>
  </form>


  <table>
    <thead>
      <tr class="title1">
        <th>Sl No.</th>
        <th>Features</th>
      </tr>
    </thead>

    <tbody>
      <?php $i = 1; while ($row = mysqli_fetch_array($f_)) { ?>
        <tr class="attri1">
          <td> <?php echo $i; ?> </td>
          <td class="projectname"> <?php echo $row['feature']; ?> 
              <a class="del_a"  href="proj1_details.php?del_task1=<?php echo $row['id'] ?>">x</a>
          </td>
        </tr>
      <?php $i++; } ?>  
    </tbody>
  </table>



  <form method="post" action="proj1_details.php" >
   <?php if (isset($_POST['cons'])) 
          {
            if (isset($errors)) 
            { ?>
              <p><?php echo $errors; ?></p>
    <?php   }
          } ?>
    <p class="ad">ADVANTAGES</p>
    <input type="text" name="advantages_" class="task_input2" placeholder="Enter Advantages">
    <button type="submit" name="cons" id="add_btn2" class="add_btn2">Add</button>
  </form>


<table>
    <thead>
      <tr class="title2">
        <th>Sl No.</th>
        <th>Advantages</th>
      </tr>
    </thead>

    <tbody>
      <?php $i = 1; while ($row = mysqli_fetch_array($ad_)) { ?>
        <tr class="attri2">
          <td> <?php echo $i; ?> </td>
          <td class="projectname"> <?php echo $row['cons']; ?>
                <a class="del_a"  href="proj1_details.php?del_task2=<?php echo $row['id'] ?>">x</a>
           </td>
        </tr>
      <?php $i++; } ?>  
    </tbody>
  </table>



  <form method="post" action="proj1_details.php">
    <?php if (isset($_POST['issues'])) 
          {
            if (isset($errors)) 
            { ?>
              <p><?php echo $errors; ?></p>
    <?php   }
          } ?>
    <p class="iss">ISSUES</p>
    <input type="text" name="issues_" class="task_input3" placeholder="Enter Issues">
    <button type="submit" name="issues" id="add_btn3" class="add_btn3">Add</button>
  </form>

  <table>
    <thead>
      <tr class="title3">
        <th>Sl No.</th>
        <th>Issues</th>
      </tr>
    </thead>

    <tbody>
      <?php $i = 1; while ($row = mysqli_fetch_array($iss_)) { ?>
        <tr class="attri3">
          <td> <?php echo $i; ?> </td>
          <td class="projectname"> <?php echo $row['issues']; ?> 
              <a class="del_a" href="proj1_details.php?del_task3=<?php echo $row['id'] ?>">x</a>
          </td>
        </tr>
      <?php $i++; } ?>  
    </tbody>
  </table>


  
</body>
</html>