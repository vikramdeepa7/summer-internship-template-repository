<?php 
  
  $errors = "";
  // connect to database
  $db = mysqli_connect("localhost", "root", "", "buddy");

  // insert a quote if submit button is clicked
  if (isset($_POST['submit'])) {

    if (empty($_POST['designation']) || empty($_POST['name']) || empty($_POST['phoneno'])) 
    {
      $errors = "DON'T LEAVE IT BLANK!!!";
    }
    else
    {
      $designation = $_POST['designation'];
      $name = $_POST['name'];
      $phoneno = $_POST['phoneno'];
      $sql="SELECT max(id) as id FROM project_1";
      $ival= mysqli_query($db, $sql);
      $irow = mysqli_fetch_array($ival);
      if(is_null($irow['id']))
        $i=1;
      else
        $i=$irow['id']+1;
      $query = "INSERT INTO project_1 VALUES ('$i','$designation','$name','$phoneno')";
      mysqli_query($db, $query);
    }
  } 

  // select all tasks if page is visited or refreshed
  $project_1 = mysqli_query($db, "SELECT designation, membername, phoneno FROM project_1");

?>



<!DOCTYPE html>
<html>
<head>
  <title>Buddy</title>
  <link rel="stylesheet" type="text/css" href="project1.css">
  <link rel="icon" href="logo.png.jpg">
</head>
<body>
  <h1>BUDDY</h1>
  <h2>Do Your Task Perfectly</h2> 
  <header>
        <nav >
          <ul >
            <li><a href="project1.php" class="home">TEAM DETAILS</a></li>
            <li><a href="proj1_details.php" class="home">PROJECT DETAILS</a></li>
            <li><a href="proj1_areas.php" class="home">FUNCTIONAL AREAS</a></li>
            <li><a href="proj1_work.php" class="home">WORK</a></li>
            <li><a href="proj1_chatroom.php" class="home">CHAT ROOM</a></li>
            <li><a href="proj1_settings.php" class="home">SETTINGS</a></li>
        </nav>
  </header>
  <div class="divider"></div>
  <div class="heading">
    <h4 class="pkpk">TEAM DETAILS</h4>
  </div>
  <form method="post" action="project1.php" >
    <?php if (isset($errors)) { ?>
      <p><center><?php echo $errors; ?></center></p>
    <?php } ?>

    <div class="pro">
    <p class="pname">DESIGNATION</p>
    <input type="text" name="designation" class="task_input1" placeholder="Enter Your Designation">
    <p class="tlead">NAME</p>
    <input type="text" name="name" class="task_input2" placeholder="Enter Your Name">
    <p class="propass">PHONE NUMBER</p>
    <input type="number" name="phoneno" class="task_input3" placeholder="Enter Your Phone Number">
    <br/>
    <button type="submit" name="submit" id="add_btn" class="add_btn">Add</button>
    </div>
  </form>
  

  <table>
    <thead>
      <tr class="title">
        <th>Designation</th>
        <th>Name</th>
        <th>Phone Number</th>
      </tr>
    </thead>

    <tbody>
      <?php $i = 1; while ($row = mysqli_fetch_array($project_1)) { ?>
        <tr class="attri">
          <td class="designation"> <?php echo $row['designation']; ?> </td>
          <td class="projectname"> <?php echo $row['membername']; ?> </td>
          <td class="phoneno"> <?php echo $row['phoneno']; ?> </td>
        </tr>
      <?php $i++; } ?>  
    </tbody>
  </table>
</body>
</html>