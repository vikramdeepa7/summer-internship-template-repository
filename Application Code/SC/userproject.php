<?php 
  
  $errors = "";
  // connect to database
  $db = mysqli_connect("localhost", "root", "", "buddy");

  // insert a quote if submit button is clicked
  if (isset($_POST['submit'])) {

    if (empty($_POST['projectname']) || empty($_POST['teamleader']) || empty($_POST['password'])) 
    {
      $errors = "DON'T LEAVE IT BLANK!!!";
    }
    else
    {
      $projectname = $_POST['projectname'];
      $teamleader = $_POST['teamleader'];
      $password = $_POST['password'];
      $sql="SELECT max(id) as id FROM projects ";
      $ival= mysqli_query($db, $sql);
      $irow = mysqli_fetch_array($ival);
      if(is_null($irow['id']))
        $i=1;
      else
        $i=$irow['id']+1;
      $query = "INSERT INTO projects VALUES ('$i','$projectname','$teamleader','$password')";
      mysqli_query($db, $query);
    }
  } 

  // select all tasks if page is visited or refreshed
  $projects = mysqli_query($db, "SELECT projectname FROM projects");

?>


<!DOCTYPE html>

<html>
<head>
	<title>Buddy</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="userproject.css">
	<link rel="icon" href="logo.png.jpg">
</head>
<body>
  <h1>BUDDY</h1>
  <h2>Do Your Task Perfectly</h2> 
	<div class="divider"></div>
  
  <div class="heading">
    <h4 class="pkpk">PROJECTS</h4>
  </div>
  <div class="probtn">
    <a class="pro_btn" href="prologin.php">GO TO YOUR PROJECT</a>
  </div>
  
  <form method="post" action="userproject.php" >
    <?php if (isset($errors)) { ?>
      <p><center><?php echo $errors; ?></center></p>
    <?php } ?>

    <div class="pro">
    <p class="pname">Project Name</p>
    <input type="text" name="projectname" class="task_input1" placeholder="Enter Project Name">
    <p class="tlead">Team Leader</p>
    <input type="text" name="teamleader" class="task_input2" placeholder="Enter The Team Leader">
    <p class="propass">Project Password</p>
    <input type="password" name="password" class="task_input3" placeholder="Enter Project Password">
    <br/>
    <button type="submit" name="submit" id="add_btn" class="add_btn">Add Project</button>
    </div>
  </form>
  

  <table>
    <thead>
      <tr class="title">
        <th>Sl No.</th>
        <th>Name Of The Project</th>
      </tr>
    </thead>

    <tbody>
      <?php $i = 1; while ($row = mysqli_fetch_array($projects)) { ?>
        <tr class="attri">
          <td> <?php echo $i; ?> </td>
          <td class="projectname"> <?php echo $row['projectname']; ?> </td>
        </tr>
      <?php $i++; } ?>  
    </tbody>
  </table>

</body>
</html>
