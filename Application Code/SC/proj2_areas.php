<?php 
  
  $errors = "";
  // connect to database
  $db = mysqli_connect("localhost", "root", "", "buddy");

  // insert a quote if submit button is clicked
  if (isset($_POST['submit'])) {

    if (empty($_POST['team'])) 
    {
      $errors = "DON'T LEAVE IT BLANK!!!";
    }
    else
    {
      $team = $_POST['team'];
      $sql="SELECT max(id) as id FROM project_1_area";
      $ival= mysqli_query($db, $sql);
      $irow = mysqli_fetch_array($ival);
      if(is_null($irow['id']))
        $i=1;
      else
        $i=$irow['id']+1;
      $query = "INSERT INTO project_1_area VALUES ('$i','$team')";
      mysqli_query($db, $query);
    }
  } 

  // select all tasks if page is visited or refreshed
  $project_1_area = mysqli_query($db, "SELECT team FROM project_1_area");

?>



<!DOCTYPE html>
<html>
<head>
	<title>Buddy</title>
	<link rel="stylesheet" type="text/css" href="proj1_areas.css">
	<link rel="icon" href="logo.png.jpg">
</head>
<body>
  <h1>BUDDY</h1>
  <h2>Do Your Task Perfectly</h2> 
  <header>
        <nav >
          <ul >
          	<li><a href="project1.php" class="home">TEAM DETAILS</a></li>
          	<li><a href="proj1_details.php" class="home">PROJECT DETAILS</a></li>
          	<li><a href="proj1_areas.php" class="home">FUNCTIONAL AREAS</a></li>
            <li><a href="proj1_work.php" class="home">WORK</a></li>
          	<li><a href="proj1_chatroom.php" class="home">CHAT ROOM</a></li>
          	<li><a href="proj1_settings.php" class="home">SETTINGS</a></li>
        </nav>
	</header>
	<div class="divider"></div>
  <div class="heading">
    <h4 class="pkpk">FUNCTIONAL AREAS</h4>
  </div>
  <form method="post" action="proj1_areas.php" >
    <?php if (isset($errors)) { ?>
      <p><center><?php echo $errors; ?></center></p>
    <?php } ?>

    <div class="pro">
    
    <input type="text" name="team" class="task_input1" placeholder="Enter New Team">
    <br/>
    <button type="submit" name="submit" id="add_btn" class="add_btn">Add</button>
    </div>
  </form>
  

  <table>
    <thead>
      <tr class="title">
        <th class="s">Sl No.</th>
        <th class="t">Teams Involved</th>
      </tr>
    </thead>

    <tbody>
      <?php $i = 1; while ($row = mysqli_fetch_array($project_1_area)) { ?>
        <tr class="attri">
          <td class="i"> <?php echo $i; ?> </td>
          <td class="team"> <?php echo $row['team']; ?> </td>
        </tr>
      <?php $i++; } ?>  
    </tbody>
  </table>
  
</body>
</html>