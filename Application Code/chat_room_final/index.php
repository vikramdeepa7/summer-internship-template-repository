<?php include("config.php");include("login.php");?>
<!DOCTYPE html>
<html>
 <head>
  <script src="//code.jquery.com/jquery-latest.js"></script>
  <script src="chat.js"></script>
  <link href="chat.css" rel="stylesheet"/>
  <link rel="stylesheet" href="style.css" type="text/css" />
  <title>Group Chat</title>
 </head>
 <body onload="setInterval('chat.update()', 1000)">
<h1>BUDDY</h>
<h2>INNOVATION DRIVEN</h2>
<header>
    <br>
    <br>
        <nav >
          <ul >
          	<li><a href="project1.php" class="home">TEAM DETAILS</a></li>
          	<li><a href="proj1_details.php" class="home">PROJECT DETAILS</a></li>
          	<li><a href="proj1_areas.php" class="home">FUNCTIONAL AREAS</a></li>
            <li><a href="proj1_work.php" class="home">WORK</a></li>
          	<li><a href="proj1_settings.php" class="home">SETTINGS</a></li>
        </nav>
    </header>
    <br>
    <br>
    <br>
    <br>
  <div id="content" style="margin-top:10px;height:100%;">
   <center><h1>Group Chat</h1></center>
   <div class="chat">
    <div class="users">
     <?php include("users.php");?>
    </div>
    <div class="chatbox">
     <?php
     if(isset($_SESSION['user'])){
      include("chatbox.php");
     }else{
      $display_case=true;
      include("login.php");
     }
     ?>
    </div>
   </div>
  </div>
 </body>
</html>
